<?php
/**
 * Route Mapper plugin for Craft CMS 3.x
 *
 * Map routes
 *
 * @link      vaersaagod.no
 * @copyright Copyright (c) 2019 Isaac and Alex
 */

/**
 * Route Mapper en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('route-mapper', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Isaac and Alex
 * @package   RouteMapper
 * @since     1.0.0
 */
return [
    'Route Mapper plugin loaded' => 'Route Mapper plugin loaded',
];
