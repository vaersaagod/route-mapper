/**
 * Route Mapper plugin for Craft CMS
 *
 * Route Mapper JS
 *
 * @author    Isaac and Alex
 * @copyright Copyright (c) 2019 Isaac and Alex
 * @link      vaersaagod.no
 * @package   RouteMapper
 * @since     1.0.0
 */
