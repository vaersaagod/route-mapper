import Map from './Map';
require('./RouteMapperField.scss');
/**
 * Route Mapper plugin for Craft CMS
 *
 * RouteMapperField Field JS
 *
 * @author    Isaac and Alex
 * @copyright Copyright (c) 2019 Isaac and Alex
 * @link      vaersaagod.no
 * @package   RouteMapper
 * @since     1.0.0RouteMapperRouteMapperField
 */

 ;(function ( $, window, document, undefined ) {

    var pluginName = "RouteMapperRouteMapperField",
        defaults = {
        };

    // Plugin constructor
    function Plugin( element, options ) {
        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init: function(id) {
            var _this = this;

            $(function () {
                Map(_this.options, _this.element).init();
            });
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );
