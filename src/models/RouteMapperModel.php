<?php
/**
 * Route Mapper plugin for Craft CMS 3.x
 *
 * Map routes
 *
 * @link      vaersaagod.no
 * @copyright Copyright (c) 2019 Isaac and Alex
 */

namespace vaersaagod\routemapper\models;

use vaersaagod\routemapper\RouteMapper;

use Craft;
use craft\base\Model;

/**
 * RouteMapperModel Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Isaac and Alex
 * @package   RouteMapper
 * @since     1.0.0
 */
class RouteMapperModel extends Model
{

    // Public Properties
    // =========================================================================

    /** @var float */
    public $duration = 0;

    /** @var float */
    public $bufferAmount = 10;

    /** @var float */
    public $distance = 0;

    /** @var array|null */
    public $route;

    /** @var array|null */
    public $waypoints;

    /** @var string */
    public $geojson = '';

    /** @var array|null */
    public $bounds = [[66.04780078589076,7.1083863583036395],[57.85033721077256,13.875512508800517]];

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */

    public function __construct($value = '[]')
    {
        parent::__construct(json_decode($value, true));
    }

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        return $rules;
    }
}
