import { meta, buffer, union, polygon } from '@turf/turf';

export default (options, element) => {
    const mapContainer = document.querySelector('[data-map]');
    const markers = [];
    const bufferSlider = element.querySelector('[data-buffer]');
    const bufferOutput = element.querySelector('[data-buffer-preview');
    let waypoints = options.value.waypoints || [];
    let bufferAmount = options.value.bufferAmount || 10;
    let route;
    let map;
    let geoJson = '';
    let shouldSave = false;

    const createRouter = () => {
        // Should be in config
        const router = L.Routing.osrmv1({
            serviceUrl: options.osrmUrl
        });

        const plan = L.Routing.plan(waypoints, {
            createMarker: (i, wp) => {
                return L.marker(wp.latLng, {
                    draggable: true,
                });
            },
            routeWhileDragging: true
        });

        const updateRouteData = () => {
            waypoints = route.inputWaypoints.map(wp => [wp.latLng.lat, wp.latLng.lng]);

            // Prep route coordinates for simplifying
            const mapped = route.coordinates.map(c => ({ x: c.lat, y: c.lng }));

            // Simplify coordinates
            const simplifiedToStore = L.LineUtil.simplify(mapped, 0.00005).map(c => [c.x, c.y]);

            const simplifiedToExtrude = L.LineUtil.simplify(mapped, 0.01);

            const simplifiedLatLng = simplifiedToExtrude.map(c => ({ lat: c.x, lng: c.y }));

            // Create geoJSON from route line coordinates
            const geoLine = L.polyline(simplifiedLatLng).toGeoJSON();

            // Extrude polygon from geoLine
            const buffered = buffer(geoLine, bufferAmount, { steps: 1 });

            // Close any holes in the polygon
            const unionized = union.apply(this, buffered.geometry.coordinates.map(c => polygon([c])));

            meta.coordEach(unionized, p => {
                p[0] = Math.round(p[0] * 1e3) / 1e3;
                p[1] = Math.round(p[1] * 1e3) / 1e3;
            });

            // Remove old polygon if it exists
            if (geoJson) {
                geoJson.remove();
            }

            // Draw the polygon
            geoJson = L.geoJSON(unionized, {
                style: feature => {
                    return { color: 'green', interactive: false };
                }
            }).addTo(map);

            if (shouldSave) {
                const bounds = geoJson.getBounds();
                const northWest = bounds.getNorthWest();
                const southEast = bounds.getSouthEast();

                const returnData = {
                    duration: route.summary.totalTime,
                    distance: route.summary.totalDistance,
                    route: simplifiedToStore,
                    waypoints: waypoints,
                    bufferAmount: bufferAmount,
                    geojson: unionized,
                    bounds: [[northWest.lat, northWest.lng], [southEast.lat, southEast.lng]]
                };

                document.getElementById(options.namespace).value = JSON.stringify(returnData);
            } else {
                shouldSave = true;
                map.fitBounds(geoJson.getBounds());
            }
        };

        const control = L.Routing.control({
            router,
            show: false,
            collapsible: false,
            plan,
            fitSelectedRoutes: false,
            routeWhileDragging: true
        }).on('routeselected', e => {
            route = e.route;
            updateRouteData();
            
        }).addTo(map);

        const onBufferChange = e => {
            console.log(e);
            bufferAmount = bufferSlider.value
            bufferOutput.innerHTML = bufferAmount;
            updateRouteData();
        };

        bufferSlider.addEventListener('change', onBufferChange);
    };

    const onMapClick = e => {
        waypoints.push([e.latlng.lat, e.latlng.lng]);

        const marker = L.marker(e.latlng).addTo(map);
        markers.push(marker);

        if (waypoints.length > 1) {
            markers.forEach(m => m.remove());
            map.off('click', onMapClick);
            createRouter();
        }
    };

    const createMap = () => {
        map = L.map(mapContainer);

        map.fitBounds(options.value.bounds);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        if (!options.osrmUrl) {
            return;
        }

        // Get waypoints from db
        if (waypoints.length < 2) {
            shouldSave = true;
            map.on('click', onMapClick);
        } else {
            createRouter();
        }
    }

    const init = () => {
        if (mapContainer.offsetWidth > 0) {
            console.log(element);
            bufferOutput.innerHTML = bufferSlider.value;
            createMap();
        } else {
            setTimeout(init, 250);
        }
    };

    const destroy = () => {

    };

    return {
        init,
        destroy
    };
};
