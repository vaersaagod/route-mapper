<?php
/**
 * Route Mapper plugin for Craft CMS 3.x
 *
 * Map routes
 *
 * @link      vaersaagod.no
 * @copyright Copyright (c) 2019 Isaac and Alex
 */

namespace vaersaagod\routemapper\models;

use craft\base\Model;

/**
 * RouteMapper Settings Model
 *
 * @author    Isaac and Alex
 * @package   RouteMapper
 * @since     1.0.0
 */
class Settings extends Model
{
    /**
     * @var string|null
     */
    public $osrmUrl;
}
